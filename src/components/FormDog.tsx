import { Dog } from "@/entities"
import { FormEvent, useState } from "react"

interface Props {
    onSubmit:(dog:Dog) => void
}

export default function FormDog({onSubmit}:Props) {


    const [errors, setErrors] = useState('');

    const [dog, setDog] = useState<Dog>({
        name: '',
        breed: '',
        birthdate: ''
    });

    function handleChange(event: any) {
        setDog({
            ...dog,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        try {
            onSubmit(dog);

        } catch(error:any) {
            if(error.response.status == 400) {
                setErrors(error.response.data.detail);
            }
        }

        

    }


    return (
        <form onSubmit={handleSubmit}>
            {errors && <p>{errors}</p>}
            <label htmlFor="name">Name :</label>
            <input type="text" name="name" value={dog.name} onChange={handleChange} required />
            <label htmlFor="breed">Breed :</label>
            <input type="text" name="breed" value={dog.breed} onChange={handleChange} />
            <label htmlFor="birthdate">Birthdate :</label>
            <input type="date" name="birthdate" value={dog.birthdate} onChange={handleChange} required />

            <button>Submit</button>
        </form>
    )
}
