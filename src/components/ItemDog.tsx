import { Dog } from "@/entities"


interface Props{
  dog:Dog;
}

export default function showDog ({dog}:Props){


    return(
        <article>
            <h3>{dog.name}</h3>
            <p>breed: {dog.breed}</p>
            <p>birthdate: {dog.birthdate}</p>
        </article>

    );
}

