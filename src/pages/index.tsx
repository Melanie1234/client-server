import ItemDog from "@/components/ItemDog";
import { fetchAllDogs, fetchOneDog } from "@/dog-service";
import { Dog } from "@/entities";
import { useEffect, useState } from "react";



export default function Index() {

    const [dogs, setDogs] = useState<Dog[]>([]);
    const [dog, setDog] = useState<Dog[]>([]);

    useEffect(() => {

        fetchAllDogs().then(data => {
            setDogs(data);
        });

    }, [])

    // useEffect(() => {

    //     fetchOneDog(2).then(data => {
    //         setDog(data);
    //         console.log(data);
            
    //     });

    // }, [])

    return (
        <>
            <h1>List of dogs</h1>
            {dogs.map(item =>
                <ItemDog key={item.id} dog={item} />
            )}

            



            {/* <h2>{ItemDog.name}</h2>
            <p>{dog.}</p>
 */}


        </>
    );
}
