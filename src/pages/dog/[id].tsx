import { fetchOneDog } from "@/dog-service";
import { Dog } from "@/entities";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export default function DogPage() {
    const router = useRouter();
    const { id } = router.query;
    const [dog, setDog] = useState<Dog>();

    useEffect(() => {
        if (!id) {
            return;
        }
        fetchOneDog(Number(id))
            .then(data => setDog(data))
            .catch(error => {
                console.log(error);
                if(error.response.status == 404) {
                    router.push('/404');
                }
            });


    }, [id]);

    if(!dog) {
       return <p>Loading...</p>
    }

    return (
        <>
            <h1>Dog {dog?.name}</h1>
            <p>Breed : {dog?.breed}</p>

        </>
    );
}
