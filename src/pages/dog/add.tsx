import FormDog from "@/components/FormDog";
import { postDog } from "@/dog-service";
import { Dog } from "@/entities";
import { useRouter } from "next/router";



export default function AddDog() {
    const router = useRouter();

    async function addDog(dog: Dog) {
        const added = await postDog(dog);
        router.push('/dog/' + added.id);
    }

    return (
        <>
            <h1>Add Dog</h1>
            <FormDog onSubmit={addDog} />
        </>
    );
}
