import axios from "axios";
import { Dog } from "./entities";



export async function fetchAllDogs() {
    const response = await axios.get<Dog[]>('http://localhost:8000/api/dog');
    return response.data;
}


export async function fetchOneDog(id:number) {
    
    const response = await axios.get<Dog>('http://localhost:8000/api/dog/'+id);
    return response.data;
}

export async function postDog(dog:Dog) {
    
    const response = await axios.post<Dog>('http://localhost:8000/api/dog/', dog);
    return response.data;
}
